package com.example.ambro.carta;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by ambro on 9/11/17.
 */

public class CardsAdapter extends ArrayAdapter<Cartas> {

    public CardsAdapter(@NonNull Context context, int resource, @NonNull List<Cartas> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Cartas card = getItem(position);

        if (convertView == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.lv_cartas_row, parent, false);
        }

        TextView nombre = convertView.findViewById(R.id.txtNombre);
        ImageView image = convertView.findViewById(R.id.ivCarta);

        nombre.setText(card.getNombre());
        Glide.with(getContext()).load(card.getImageUrl()).into(image);

        return convertView;
    }
}
