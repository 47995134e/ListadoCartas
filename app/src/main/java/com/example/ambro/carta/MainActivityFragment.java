package com.example.ambro.carta;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private ArrayList<Cartas> cartas;
    private CardsAdapter adapter;

    public MainActivityFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        ListView lvcartas = (ListView) view.findViewById(R.id.lvCartas);





        cartas = new ArrayList<>();


        adapter = new CardsAdapter(
        getContext(),
                R.layout.lv_cartas_row,
                cartas
        );

        lvcartas.setAdapter(adapter);
        lvcartas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Cartas item = (Cartas) adapterView.getItemAtPosition(i);
//adsfasfdsafsfasfasfsafdsafsadfsdafdsfsadfsdfsadfsdafsadfasfasfdsafasfdsafsaddsfsdafsdfsdaf
                Intent intent = new Intent(getContext(), DetailActivity.class);
                intent.putExtra("card", item);

                startActivity(intent);
            }
        });

        return view;
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_cartas_fragment,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_refresh){
            refresh();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        refresh();
    }

    private void refresh() {

        refreshDataTask task = new refreshDataTask();
        task.execute();
    }

    private class refreshDataTask extends AsyncTask<Void,Void,ArrayList<Cartas>>{

        @Override
        protected ArrayList<Cartas> doInBackground(Void... voids) {
            MagicTheGatheringAPI api = new MagicTheGatheringAPI();
            //SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());



            ArrayList<Cartas> result = api.getCards();

            Log.d("DEBUG", result.toString());

            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<Cartas> cartas) {
            adapter.clear();
            for (Cartas carta : cartas) {
                adapter.add(carta);
            }
        }
    }
}
