package com.example.ambro.carta;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ambro on 21/10/17.
 */

public class Cartas implements Serializable{
    private String nombre;
    private String manaCost;
    ArrayList<String> colors;
    private String rarity;
    private String text;
    private String imageUrl;
    /*
    private String coloridentity;
    private String type;
    private String supertypes;
    private String types;
    private String subtypes;

    private String set;

    private String artist;
    private String number;
    private String power;
    private String toughness;
    private String layout;
    private int multiverseid;
    private String imageUrl;
    private String date;
    private String name;
    private String language;
    private String printings;
    private String originalText;
    private String originalType;
    private String id;
   */
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getManaCost() {
        return manaCost;
    }

    public void setManaCost(String manaCost) {
        this.manaCost = manaCost;
    }

    public ArrayList<String> getColors() {
        return colors;
    }

    public void setColors(ArrayList<String> colors) {
        this.colors = colors;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /*
    public String getColoridentity() {
        return coloridentity;
    }

    public void setColoridentity(String coloridentity) {
        this.coloridentity = coloridentity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSupertypes() {
        return supertypes;
    }

    public void setSupertypes(String supertypes) {
        this.supertypes = supertypes;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public String getSubtypes() {
        return subtypes;
    }

    public void setSubtypes(String subtypes) {
        this.subtypes = subtypes;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public String getSet() {
        return set;
    }

    public void setSet(String set) {
        this.set = set;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getToughness() {
        return toughness;
    }

    public void setToughness(String toughness) {
        this.toughness = toughness;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public int getMultiverseid() {
        return multiverseid;
    }

    public void setMultiverseid(int multiverseid) {
        this.multiverseid = multiverseid;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPrintings() {
        return printings;
    }

    public void setPrintings(String printings) {
        this.printings = printings;
    }

    public String getOriginalText() {
        return originalText;
    }

    public void setOriginalText(String originalText) {
        this.originalText = originalText;
    }

    public String getOriginalType() {
        return originalType;
    }

    public void setOriginalType(String originalType) {
        this.originalType = originalType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
 */

    public Cartas() {
    }

    public Cartas(String nombre, String imageUrl) {
        this.nombre = nombre;
        this.imageUrl = imageUrl;
    }

    public Cartas(String nombre, String manaCost, ArrayList<String> colors, String rarity, String text) {
        this.nombre = nombre;
        this.manaCost = manaCost;
        this.colors = colors;
        this.rarity = rarity;
        this.text = text;
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "Cartas{" +
                "nombre='" + nombre + '\'' +
                ", manaCost=" + manaCost +
                ", colors=" + colors +
                ", rarity='" + rarity + '\'' +
                ", text='" + text + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
