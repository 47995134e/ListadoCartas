package com.example.ambro.carta;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by ambro on 21/10/17.
 */

public class MagicTheGatheringAPI {

    //private final String API_KEY = "1ea3bf746c81fe337d4cf49e7e66d670";

    ArrayList<Cartas> getCards(){

        String base_url = "https://api.magicthegathering.io/v1";

        Uri builturl= Uri.parse(base_url)
                .buildUpon()
                .appendPath("cards")
                .build();

        String url = builturl.toString();
        Log.e("URL", url);


        return doCall(url);
    }

    private ArrayList<Cartas> doCall(String url) {
        try {
            String JsonResponse =HttpUtils.get(url);
            Log.e("RESPONSE", JsonResponse);

            return processJson(JsonResponse);
        }catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<Cartas> processJson(String JsonResponse) {
        ArrayList<Cartas> Cartas = new ArrayList<>();
        try {
            JSONObject data = new JSONObject(JsonResponse);
            JSONArray jsonCartass = data.getJSONArray("cards");
            for (int i = 0; i < jsonCartass.length(); i++) {
                JSONObject jsoncarta = jsonCartass.getJSONObject(i);

                Cartas carta = new Cartas();
                carta.setNombre(jsoncarta.getString("name"));
                //carta.setColors(jsoncarta.getString(toString("color")));
                carta.setImageUrl(jsoncarta.getString("imageUrl"));
                carta.setManaCost(jsoncarta.getString("manaCost"));
                carta.setRarity(jsoncarta.getString("rarity"));
                carta.setText(jsoncarta.getString("text"));
                Log.d("DEBUG", carta.toString());
                Cartas.add(carta);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return Cartas;
    }
    
}
