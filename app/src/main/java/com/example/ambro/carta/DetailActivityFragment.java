package com.example.ambro.carta;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetailActivityFragment extends Fragment {

    private View view;
    private TextView text;
    private TextView nombre;
    private TextView texto;
    private TextView manacost;
    private TextView rarity;
    private ImageView foto;

    public DetailActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_detail, container, false);
        Intent i = getActivity().getIntent();

        //ImageView imagen = view.
        text = view.findViewById(R.id.txtNombre);

        if(i !=null){
            Cartas card = (Cartas) i.getSerializableExtra("card");

            if (card != null) {
                showData(card);
            }
        }

        return view;
    }

    private void showData(Cartas card) {
        nombre = view.findViewById(R.id.tvDetail);
        foto = view.findViewById(R.id.ivDetail);
        texto = view.findViewById(R.id.tvDetailtxt);
        manacost = view.findViewById(R.id.tvDetailmanacost);
        rarity = view.findViewById(R.id.tvDetailrarity);

        nombre.setText(card.getNombre());

        texto.setText(card.getText());
        manacost.setText(card.getManaCost());
        rarity.setText(card.getRarity());
        Glide.with(getContext()).load(card.getImageUrl()).into(foto);

    }
}
